"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

from io import open
from os import path

from setuptools import find_packages, setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='fetenkiste',

    version='1.0.0',

    description='Fotobooth implementation for the RPi',

    long_description=long_description,

    url='https://bitbucket.org/frairon/fetenkiste',

    keywords='rpi raspberry camera',

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    package_data={
        'fetenkiste': ['images/fotobox-%d.png' % x for x in range(8)],
    },

    install_requires=['futures',
                      'pyusb',
                      'pillow==2.6.1',
                      'piexif',
                      'psutil'],
)
