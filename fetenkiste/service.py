import argparse
import logging
import signal
import subprocess
import sys
import time

import fetenkiste as fk
import keyboard
import tornado
from tornado import gen

# configure root logger
logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)-5.5s]  %(message)s")
logger = logging.Logger("Controller")
logger.setLevel(logging.DEBUG)
consoleHandler = logging.StreamHandler(stream=sys.stdout)
consoleHandler.setFormatter(logging.Formatter(
    "%(asctime)s [%(levelname)-5.5s]  %(message)s"))
logger.addHandler(consoleHandler)

fkInstance = None


def startFetenkiste(io_loop, args):
    global fkInstance

    if fkInstance is None:
        logger.info("Starting fetenkiste instance")
        fkInstance = fk.main(io_loop, args)
    else:
        logger.info("Instance already running. Ignoring call")


def stopFetenkiste():
    global fkInstance
    if fkInstance:
        logger.info("Found running fetenkiste instance, shutting it down")
        fkInstance.stop()
        fkInstance = None
    else:
        logger.info("No instance running. Ignoring")
    logger.info("stopped.")


# simple semaphore to avoid sending too many keys when the
# key is simply hold down.
keySemaphore = False

@gen.coroutine
def sendKey(key):
    global fkInstance
    global keySemaphore

    if keyboard.is_modifier(key.scan_code):
        return

    if keySemaphore:
        return

    if fkInstance and fkInstance.isStopped():
        # it's stopped
        fkInstance = None

    if not fkInstance:
        return

    # logger.info("key: %s" % key.name)
    io_loop = tornado.ioloop.IOLoop.current()

    @gen.coroutine
    def doHandle(key):
        global keySemaphore

        yield fkInstance.handleInput(key)
        keySemaphore = False

    keySemaphore = True
    io_loop.call_later(0.05, doHandle, key.name)


def sig_handler(sig, frame):
    logger.warning('Caught signal: %s', sig)

    io_loop = tornado.ioloop.IOLoop.current()
    # stop fetenkiste
    io_loop.add_callback(stopFetenkiste)
    # then stop the loop
    io_loop.add_callback(io_loop.stop)


def shutdown():
    subprocess.call('sudo shutdown -h 0'.split(' '))


def restart():
    subprocess.call('sudo shutdown -r 0'.split(' '))


def release():
    if not fkInstance:
        return

    fkInstance.triggerRelease()


def toggleSettings():
    if not fkInstance:
        return

    fkInstance.toggleSettings()

def setTime():
    if not fkInstance:
        return

    fkInstance.setTime()


def main(args):
    logger.info("Fetenkiste Controller service starting up")

    io_loop = tornado.ioloop.IOLoop.current()

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    keyboard.add_hotkey('ctrl+f1', io_loop.add_callback, args=(toggleSettings,))
    keyboard.add_hotkey('ctrl+f2', io_loop.add_callback,
                        args=(startFetenkiste, io_loop, args))
    keyboard.add_hotkey('ctrl+f3', io_loop.add_callback,
                        args=(stopFetenkiste,))
    keyboard.add_hotkey('ctrl+f4', io_loop.add_callback, args=(shutdown,))
    keyboard.add_hotkey('ctrl+f5', io_loop.add_callback, args=(restart,))
    keyboard.add_hotkey('ctrl+r', io_loop.add_callback, args=(release,))
    keyboard.add_hotkey('ctrl+f6', io_loop.add_callback, args=(setTime,))

    # send every key to fetenkiste, if running
    keyboard.on_press(lambda x: io_loop.add_callback(sendKey, x))

    io_loop.call_later(2, startFetenkiste, io_loop, args)
    io_loop.start()

if __name__ == '__main__':

    parser = fk.createArgParser("Fetenkiste-Service")
    args = parser.parse_args()

    main(args)
