import argparse
import ConfigParser
import io
import logging
import os
import signal
import subprocess
import sys
import time
from datetime import datetime

from concurrent.futures import ThreadPoolExecutor
from PIL import Image, ImageDraw, ImageFont

import picamera
import pigpio
import tornado
from escpos import exceptions, printer
from tornado import gen
from tornado.concurrent import run_on_executor

pigpio.exceptions = True


class FetenKiste(object):
    executor = ThreadPoolExecutor(max_workers=3)

    MODE_FOTO = 0
    MODE_SETTINGS = 1
    MODE_TIME = 2

    # simple stop signal.
    # Will be set to true, when the system is supposed to shutdown
    _stop = False
    # will be set to false while it's running
    _stopped = True

    def __init__(self, config, args):
        self.config = config
        self.appState = ConfigParser.SafeConfigParser()
        self.args = args
        self.infoOverlay = None
        self.logger = None

        self.initLogger()

        self.mode = self.MODE_FOTO

        self.enteredTime = ""

        self.printer = None

        self.fotoSeqNum = 1
        if config.has_option('Images', 'seq_num'):
            self.fotoSeqNum = config.getint('Images', 'seq_num')
        self.fotoSeqNumOverlay = None

        self.captionTemplate = ""
        if config.has_option('Images', 'caption'):
            self.captionTemplate = config.get('Images', 'caption')

        self.printerSettings = {}
        if config.has_section('Printer'):
            self.printerSettings['resample'] = config.getint(
                'Printer', 'resample')
            self.printerSettings['sharpness'] = config.getfloat(
                'Printer', 'sharpness')
            self.printerSettings['brightness'] = config.getint(
                'Printer', 'brightness')
            self.printerSettings['contrast'] = config.getint(
                'Printer', 'contrast')

        imagePath = lambda img: os.path.join(
            os.path.abspath(os.path.dirname(__file__)), "images", img)

        self.overlays = {
            'timer-1': imagePath('fotobox-0.png'),
            'timer-2':  imagePath('fotobox-6.png'),
            'timer-3':  imagePath('fotobox-7.png'),
            'timer-end':  imagePath('fotobox-1.png'),
            'blank':  imagePath('fotobox-1.png'),
        }

        self.logger.info("Creating Image target directory (if not exists)")
        self.targetDir = self.config.get('Images', 'target_location')
        if not os.path.exists(self.targetDir):
            os.makedirs(self.targetDir)

    def initLogger(self):
        logFormatter = logging.Formatter(
            "%(asctime)s [%(levelname)-5.5s]  %(message)s")
        self.logger = logging.getLogger("FetenKiste")

        fileHandler = logging.FileHandler(os.path.join(
            self.args.logDir, datetime.now().strftime("fetenkiste-%Y-%m-%d.log")))
        fileHandler.setFormatter(logFormatter)
        self.logger.addHandler(fileHandler)

    def initHardware(self):
        self.logger.info("Initializing buttons etc.")
        self.hardware = pigpio.pi()
        if self.hardware is None or not self.hardware.connected:
            raise Exception("Error inizializing hardware")
        self.logger.info("your pi hardware is {}".format(
            self.hardware.get_hardware_revision()))

        self.releaseButton = self.config.getint('Hardware', 'release_button')
        self.hardware.set_mode(self.releaseButton, pigpio.INPUT)

        self.initCamera()
        if not self.args.noPrinter:
            self.initPrinter()

    def toggleSettings(self):
        if self.mode == self.MODE_SETTINGS:
            self.mode = self.MODE_FOTO
        else:
            self.mode = self.MODE_SETTINGS

        if self.mode == self.MODE_SETTINGS:
            self.updateInfoOverlay()
        else:
            self.saveAppState()
            self.removeInfoOverlay()

    def getState(self):
        return {
            'camera': dict(
                brightness=self.cam.brightness,
                awb_mode=self.cam.awb_mode,
                exposure_mode=self.cam.exposure_mode,
                iso=self.cam.iso,
                contrast=self.cam.contrast,
                sharpness=self.cam.sharpness,
                saturation=self.cam.saturation,
                exposure_compensation=self.cam.exposure_compensation,
                awb_gains=self.cam.awb_gains
            ),
            'printer': self.printerSettings,
        }

    @gen.coroutine
    def handleInput(self, key):

        if self.mode == self.MODE_SETTINGS:
            self.handleSettingsInput(key)
        elif self.mode == self.MODE_TIME:
            self.handleTimeInput(key)
        else:
            self.removeInfoOverlay()

    @gen.coroutine
    def handleSettingsInput(self, key):
        if key == "esc":
            self.toggleSettings()
            return

        if key == "w":
            if self.cam.awb_mode not in self.awbmodes:
                awbIdx = 0
            else:
                awbIdx = (self.awbmodes.index(
                    self.cam.awb_mode) + 1) % len(self.awbmodes)

            self.cam.awb_mode = self.awbmodes[awbIdx]
        if key == "e":
            if self.cam.exposure_mode not in self.exposureModes:
                idx = 0
            else:
                idx = (self.exposureModes.index(
                    self.cam.exposure_mode) + 1) % len(self.exposureModes)

            self.cam.exposure_mode = self.exposureModes[idx]
        if key == "r":
            if self.cam.iso not in self.isoModes:
                idx = 0
            else:
                idx = (self.isoModes.index(
                    self.cam.iso) + 1) % len(self.isoModes)

            self.cam.iso = self.isoModes[idx]
        # brightness
        if key == "f":
            self.cam.brightness = min(self.cam.brightness + 1, 100)
        if key == "v":
            self.cam.brightness = max(self.cam.brightness - 1, 0)
        # contrast
        if key == "d":
            self.cam.contrast = min(self.cam.contrast + 1, 100)
        if key == "c":
            self.cam.contrast = max(self.cam.contrast - 1, 0)
        # sharpness
        if key == "l":
            self.cam.sharpness = min(self.cam.sharpness + 5, 100)
        if key == ".":
            self.cam.sharpness = max(self.cam.sharpness - 5, -100)
        # saturation
        if key == "o":
            self.cam.saturation = min(self.cam.saturation + 5, 100)
        if key == "p":
            self.cam.saturation = max(self.cam.saturation - 5, -100)
        # exposure compensation
        if key == "x":
            self.cam.exposure_compensation = min(
                self.cam.exposure_compensation + 1, 25)
        if key == "y":
            self.cam.exposure_compensation = max(
                self.cam.exposure_compensation - 1, -25)

        def changeAwbGain(deltaRed, deltaBlue):
            red, blue = self.cam.awb_gains
            red = min(max(red + deltaRed, 0.0), 8.0)
            blue = min(max(blue + deltaBlue, 0.0), 8.0)
            self.cam.awb_gains = (red, blue)
            self.cam.awb_mode = 'off'

        # white balance red
        if key == "j":
            changeAwbGain(0.1, 0.0)
        if key == "m":
            changeAwbGain(-0.1, 0.0)
        # whtie balance blue
        if key == "k":
            changeAwbGain(0.0, 0.1)
        if key == ",":
            changeAwbGain(0.0, -0.1)

        # printer settings
        if key == "g":
            self.printerSettings['brightness'] = min(
                self.printerSettings['brightness'] + 1, 100)
        if key == "b":
            self.printerSettings['brightness'] = max(
                self.printerSettings['brightness'] - 1, -100)
        if key == "h":
            self.printerSettings['contrast'] = min(
                self.printerSettings['contrast'] + 1, 100)
        if key == "n":
            self.printerSettings['contrast'] = max(
                self.printerSettings['contrast'] - 1, -100)
        if key == "t":
            self.printerSettings['contrast'] = min(
                self.printerSettings['contrast'] + 1, 100)
        if key == "z":
            self.printerSettings['contrast'] = max(
                self.printerSettings['contrast'] - 1, -100)

        # reset sequence number
        if key == '#':
            self.fotoSeqNum = 1
            self.showFotoSeqNumOverlay()

        self.updateInfoOverlay()

    def setTime(self):
        self.logger.info("Entering time-mode")
        self.mode = self.MODE_TIME
        self.enteredTime = ""
        self.updateTimeOverlay()

    def handleTimeInput(self, key):
        self.updateTimeOverlay()

        key = str(key)
        if key in "1234567890":
            if len(self.enteredTime) >= 12:
                self.enteredTime = ""

            self.enteredTime = self.enteredTime + key
        elif key == "esc":
            self.logger.info("time-mode aborted")
            self.mode = self.MODE_FOTO
            self.removeInfoOverlay()
            return
        elif key == "backspace":
            if len(self.enteredTime) > 0:
                self.enteredTime = self.enteredTime[:-1]
        elif key == "enter":
            if len(self.enteredTime) == 12:
                t = self.enteredTime
                time = "%s-%s-%s %s:%s:00" % (t[4:8],
                                              t[2:4], t[0:2], t[8:10], t[10:12])
                self.logger.info("resetting time to %s" % time)
                try:
                    subprocess.check_call(["timedatectl", "set-time", time])
                except Exception as e:
                    self.logger.info("Error setting time: %s" % e)
                self.mode = self.MODE_FOTO
                self.removeInfoOverlay()
                return

        self.updateTimeOverlay()

    def updateTimeOverlay(self):
        pad = Image.new('RGBA', self.resolution)
        pad.paste((0, 0, 0, 0), (0, 0) + self.resolution)
        d = ImageDraw.Draw(pad)
        font = ImageFont.truetype(
            font="/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf", size=30)
        color=(100, 100, 255)
        d.text((25, 100), "Set Date and Time", font=font, fill=color)
        d.text((25, 150), "Current Time %s" % datetime.now().strftime(
            "%d.%m.%Y %H:%M"), font=font, fill=color)

        t = self.enteredTime
        if len(t) < 12:
            t = t + "_"
            t = t.ljust(12, '?')
        d.text((25, 200), "New Time     %s.%s.%s %s:%s" % (
            t[0:2], t[2:4], t[4:8], t[8:10], t[10:12]), font=font, fill=color)
        d.text((25, 300), "Back: [Escape], Ok: [Enter]",
               font=font, fill=color)

        self.replaceOverlay(pad)

    def saveAppState(self):
        if not self.args.applicationState:
            self.logger.info("no application-state-file configured.")
            return

        for section in ['Camera', 'Images', 'Printer']:
            if not self.appState.has_section(section):
                self.appState.add_section(section)

        start = time.time()
        self.appState.set('Camera', 'brightness', str(self.cam.brightness))
        self.appState.set('Camera', 'contrast', str(self.cam.contrast))
        self.appState.set('Camera', 'sharpness', str(self.cam.sharpness))
        self.appState.set('Camera', 'saturation', str(self.cam.saturation))
        self.appState.set('Camera', 'iso', str(self.cam.iso))
        self.appState.set('Camera', 'exposure_mode',
                          str(self.cam.exposure_mode))
        self.appState.set('Camera', 'exposure_compensation',
                          str(self.cam.exposure_compensation))
        self.appState.set('Camera', 'awb_mode', str(self.cam.awb_mode))
        self.appState.set('Camera', 'awb_gains', "%.4f,%.4f" % (
            (self.cam.awb_gains[0], self.cam.awb_gains[1])))
        self.appState.set('Images', 'seq_num', str(self.fotoSeqNum))
        self.appState.set('Printer', 'resample', str(
            self.printerSettings['resample']))
        self.appState.set('Printer', 'sharpness', str(
            self.printerSettings['sharpness']))
        self.appState.set('Printer', 'brightness', str(
            self.printerSettings['brightness']))
        self.appState.set('Printer', 'contrast', str(
            self.printerSettings['contrast']))

        self.logger.debug("writing app state")
        with open(self.args.applicationState, 'w') as out:
            self.appState.write(out)

    def updateInfoOverlay(self):
        # Create an image padded to the required size with
        # mode 'RGB'

        pad = Image.new('RGBA', self.resolution)
        pad.paste((0, 0, 0, 0), (0, 0) + self.resolution)
        d = ImageDraw.Draw(pad)
        font = ImageFont.truetype(
            font="/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf", size=21)
        # font="/usr/share/fonts/truetype/freefont/FreeMono.ttf", size=21)

        features = [
            ("Exit Settings             (ESC)", ""),
            ("Foto Number               (reset:#)", self.fotoSeqNum),
            ("Resolution", self.resolution),
            ("Brightness    [0..100]    (F/V)", self.cam.brightness),
            ("Contrast      [0..100]    (D/C)", self.cam.contrast),
            ("Sharpness     [-100..100] (L/.)", self.cam.sharpness),
            ("Saturation    [-100..100] (O/P)", self.cam.saturation),
            ("ISO           [0=auto]    (R)", ' '.join(
                [str(mode) if mode != self.cam.iso else "[%d]" % mode for mode in self.isoModes])),
            ("Exposure Modes            (E)", self.cam.exposure_mode),
            ("   modes: ", ' '.join([mode if mode != self.cam.exposure_mode else "[" +
                                     mode + "]" for mode in self.exposureModes[:len(self.exposureModes) / 2]])),
            ("", ' '.join([mode if mode != self.cam.exposure_mode else "[" +
                           mode + "]" for mode in self.exposureModes[len(self.exposureModes) / 2:]])),
            ("Exposure Comp [-25..25]   (X/Y)",
             self.cam.exposure_compensation),
            ("Auto white balance        (W)", self.cam.awb_mode),
            ("   modes: " + (' '.join([mode if mode != self.cam.awb_mode else "[" +
                                       mode + "]" for mode in self.awbmodes])), ""),
            ("White balance values:", ""),
            ("   red        [0.0-8.0]   (J/M)", "%.3f" %
             float(self.cam.awb_gains[0])),
            ("   blue       [0.0-8.0]   (K/,)", "%.3f" %
             float(self.cam.awb_gains[1])),
            ("  ", ""),
            ("Printer Settings:", ""),
            ("Brightness    [-100..100]   (G/B)",
             self.printerSettings['brightness']),
            ("Contrast      [-100..100]   (H/N)",
             self.printerSettings['contrast']),
            ("  ", ""),
            ("System:", ""),
            ("Shutdown                    (F4)", ""),
            ("Restart                     (F5)", ""),
            ("Current Time                    ",
             datetime.now().strftime("%d.%m.%Y %H:%M"))
        ]
        for idx, (name, value) in enumerate(features):
            line = "%s %s" % (name.ljust(30), str(value))
            d.text((25, 100 + (idx * 30)), line, font=font, fill=(0, 0, 0))

        self.replaceOverlay(pad)

    def replaceOverlay(self, pad):
        overlay = self.cam.add_overlay(
            pad.tobytes(), format="rgba", layer=3, alpha=255, size=self.resolution)
        if self.infoOverlay is not None:
            self.cam.remove_overlay(self.infoOverlay)
        self.infoOverlay = overlay

    def removeInfoOverlay(self):
        if self.infoOverlay is not None:
            self.cam.remove_overlay(self.infoOverlay)
            self.infoOverlay = None

    def initCamera(self):

        def setCamOption(option, getter='get', formatter=None):
            if self.config.has_option("Camera", option):
                value = getattr(self.config, getter)("Camera", option)
                if formatter is not None:
                    value = formatter(value)

                setattr(self.cam, option, value)
                return value

        def setCamOptionInt(option, formatter=None):
            return setCamOption(option, getter='getint', formatter=formatter)

        def setCamOptionBoolean(option, formatter=None):
            return setCamOption(option, getter='getboolean', formatter=formatter)

        self.cam = picamera.PiCamera()
        # let the camera get up
        time.sleep(0.5)
        setCamOptionInt("brightness")
        setCamOptionInt("contrast")
        setCamOptionInt("sharpness")
        setCamOptionInt("saturation")
        setCamOptionInt("iso")
        setCamOptionInt("exposure_compensation")
        setCamOptionInt("framerate")
        setCamOptionInt("rotation")
        setCamOption("awb_mode")
        setCamOption("exposure_mode")
        setCamOptionBoolean("hflip")
        self.resolution = setCamOption("resolution", formatter=lambda x: (
            int(x.split(',')[0]), int(x.split(',')[1])))
        setCamOption("awb_gains", formatter=lambda x: (
            float(x.split(',')[0]), float(x.split(',')[1])))

        self.awbmodes = ['off'] + \
            [k for k in self.cam.AWB_MODES.keys() if k != "off"]
        self.exposureModes = [
            'off'] + [k for k in self.cam.EXPOSURE_MODES.keys() if k != "off"]
        self.isoModes = [0, 100, 200, 320, 400, 500, 640, 800]

        self.cam.start_preview()

    def getImageName(self):
        return "FetenKiste-{seq:03d}-{time}.jpg".format(seq=self.fotoSeqNum, time=time.strftime("%Y-%m-%d_%H%M%S"))

    def showFotoSeqNumOverlay(self):
        # do not show the overlay... takes too much CPU
        return

        if self.fotoSeqNumOverlay:
            self.cam.remove_overlay(self.fotoSeqNumOverlay)

        start = time.time()
        pad = Image.new('RGBA', self.resolution)
        pad.paste((0, 0, 0, 0), (0, 0) + self.resolution)
        d = ImageDraw.Draw(pad)

        # text to be drawn
        font = ImageFont.truetype(
            font="/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf", size=24)

        seqText = "[{:03d}]".format(self.fotoSeqNum)
        size = font.getsize(seqText)

        textcoords = (self.resolution[0] - 25 -
                      size[0], self.resolution[1] - 25 - size[1])

        d.text(textcoords,
               seqText, font=font, fill=(0, 0, 0))

        self.fotoSeqNumOverlay = self.cam.add_overlay(
            pad.tobytes(), format="rgba", layer=4, alpha=255, size=self.resolution,
            crop=(textcoords[0], textcoords[1], self.resolution[
                  0] - textcoords[0], self.resolution[1] - textcoords[1]),
            fullscreen=False,
            window=(textcoords[0], textcoords[1], self.resolution[
                    0] - textcoords[0], self.resolution[1] - textcoords[1]),
        )
        self.logger.info("creating overlay took %.2fms" %
                         ((time.time() - start) * 1000.0))

    @gen.coroutine
    def triggerRelease(self):
        self.logger.info("-----  releasing picture %d -----" % self.fotoSeqNum)
        yield self.release()
        self.fotoSeqNum += 1
        self.saveAppState()
        self.showFotoSeqNumOverlay()
        self.logger.debug("ready for next picture")

    @gen.coroutine
    def start(self):
        self.showFotoSeqNumOverlay()
        self._stopped = False
        self._stop = False
        while not self._stop:
            if self.hardware.read(self.releaseButton) == 0:
                yield self.triggerRelease()
            else:
                yield gen.sleep(0.05)

    def stop(self):
        self._stop = True

    def isStopped(self):
        return self._stopped

    def shutdown(self):
        self.logger.info("Shutting down Fetenkiste")
        if self._stopped:
            logging.warn("Fetenkiste not running. Ignoring the call.")
            return

        self.saveAppState()

        # might already be set to true, but maybe the main returned
        # due to an error
        self._stop = True
        # do some cleanup (camera and buttons!)
        self.logger.info("stopping hardware")
        self.hardware.stop()
        try:
            if self.cam:
                self.cam.close()
        except Exception as e:
            logging.error("Error closing camera", e)
        finally:
            self._stopped = True

    @gen.coroutine
    def showExternalOverlay(self, name, duration):
        image = self.overlays[name]
        pngView = self.config.get("Images", "pngviewer")
        try:
            overlay = subprocess.Popen(
                "{0} {1} -b 0 -l 3".format(pngView, image).split(' '))
            yield gen.sleep(duration)
        finally:
            overlay.terminate()

    @gen.coroutine
    def release(self):

        yield self.showExternalOverlay('timer-3', 1)
        yield self.showExternalOverlay('timer-2', 1)
        yield self.showExternalOverlay('timer-1', 1)
        yield self.showExternalOverlay('timer-end', 0.5)

        nextImage = self.getImageName()

        imagePath = os.path.join(self.targetDir, nextImage)

        stream = io.BytesIO()
        self.cam.capture(stream, format="jpeg",
                         use_video_port=True, quality=90)
        stream.seek(0)
        img = Image.open(stream)
        # Create an image padded to the required size with
        # mode 'RGB'
        pad = Image.new('RGB', (
            ((img.size[0] + 31) // 32) * 32,
            ((img.size[1] + 15) // 16) * 16,
        ))
        # Paste the original image into the padded one
        pad.paste(img, (0, 0))
        # save the image flipped again
        img.transpose(Image.FLIP_LEFT_RIGHT).save(imagePath, quality=90)

        # Add the overlay with the padded image as the source,
        # but the original image's dimensions
        try:
            o = self.cam.add_overlay(pad.tostring(), size=img.size)
            o.layer = 3
            self.printImage(imagePath)

            yield gen.sleep(1)
        finally:
            self.cam.remove_overlay(o)

    def initPrinter(self):
        self.logger.info("Initializing printer")
        adr = self.config.get('Printer', 'address')
        adr = adr.split(':')
        try:
            self.printer = printer.Usb(int(adr[0], base=16), int(adr[1], base=16))
        except Exception as e:
            self.logger.error("Error initializing printer: " + str(e))

    def printImage(self, imagePath):
        self.logger.info("converting image for printing")
        start = time.time()
        outputPath = '/tmp/feten-kiste-print.jpg'
        settings = "-resample {resample} -brightness-contrast {brightcontrast}"
        settings = settings.format(resample=self.printerSettings['resample'],
                                   brightcontrast="{}x{}".format(
                                       self.printerSettings['brightness'], self.printerSettings['contrast']),
                                   )
        converter = self.config.get('Printer', 'convert_preview').format(
            input=imagePath, output=outputPath, settings=settings)

        subprocess.call(converter.split(' '))
        self.logger.info("... took %.2fms" %
                         ((time.time() - start) * 1000.0))
        if self.args.noPrinter or self.printer is None:
            self.logger.info("No printer is connected. Omitting the printing")
            return

        self.logger.info("Printing image")
        start = time.time()
        self.printer.image(outputPath)
        self.printer.text(
            "\n" + self.captionTemplate.format(time=time.strftime("%H:%M:%S")) + "\n")
        self.printer.cut()
        self.logger.info("... took %.2fms" %
                         ((time.time() - start) * 1000.0))


@gen.coroutine
def run(fk):
    try:
        fk.logger.info("Starting Fetenkiste")
        fk.initHardware()
        yield fk.start()
    except Exception as e:
        fk.logger.info("Exception during execution of the fetenkiste:", e)
        raise
    finally:
        fk.logger.info("Cleaning up")
        fk.shutdown()


@gen.coroutine
def autoRelease(fk, interval=20):
    fk.logger.info("Starting auto release every {} seconds".format(interval))
    # wait 30 seconds initially
    yield gen.sleep(10)

    # then take a picture every 20 seconds forever
    while True:
        fk.logger.info("Auto releasing picture", exc_info=False)
        yield fk.triggerRelease()
        yield gen.sleep(interval)


@gen.coroutine
def writeStats():
    import psutil
    logFormatter = logging.Formatter(
        "%(asctime)s [%(levelname)-5.5s]  %(message)s")
    fileHandler = logging.FileHandler(
        datetime.now().strftime("fetenkiste-%Y-%m-%d-stats.log"))
    fileHandler.setFormatter(logFormatter)

    MB = 1024 * 1024
    statLogger = logging.Logger("stats", logging.INFO)
    statLogger.addHandler(fileHandler)

    while True:
        cpu = psutil.cpu_percent(interval=None)

        memory = psutil.virtual_memory()
        statLogger.info("CPU: %.2f Memory left: %sMb",
                        cpu, memory.available / MB)
        yield gen.sleep(10)


def main(io_loop, args):
    config = ConfigParser.SafeConfigParser(allow_no_value=True)
    config.read(args.config)

    # overwrite the config with the application state
    if args.applicationState:
        config.read(args.applicationState)

    fk = FetenKiste(config, args)

    io_loop.add_callback(run, fk)

    if args.autoRelease:
        io_loop.add_callback(autoRelease, fk, args.autoRelease)

    if args.writeStats:
        io_loop.add_callback(writeStats)

    return fk


def createArgParser(name):
    parser = argparse.ArgumentParser(name)
    parser.add_argument("config", help="configuration file")
    parser.add_argument("--application-state", dest="applicationState",
                        help="filename to store the application state")
    parser.add_argument("--log-dir", dest="logDir",
                        help="directory to write log files to", default="")
    parser.add_argument("--no-printer", dest="noPrinter",
                        action="store_true", help="Set when there's no printer.")
    parser.add_argument("--write-stats", dest="writeStats",
                        action="store_true", help="Enable writing stats to separate file for performance debugging. Requires psutil package")
    parser.add_argument("--auto-release", dest="autoRelease",
                        type=int, help="Auto release in passed interval (seconds).")

    return parser
