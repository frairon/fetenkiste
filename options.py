
from picamera import PiCamera



class EnumProperty(object):

    def __init__(self, name, propName, *options):
        self.options = options
        self.name = name
        self.propName = propName

    def getCurrent(self, cam):
        return getattr(cam, self.propName)

    def selectNext(self, cam):
        value = getattr(cam, self.propName)
        if value not in self.options:
            # return a default if nothing is selected yet
            next = self.options[0]

        next = self.options[(self.options.index(value) + 1) %
                            len(self.options)]

        setattr(cam, self.propName, next)

        return next

    # def uiText(self, cam):
    #     return "{name: <15}[{minVal: <4}...{maxVal:<4}]  (%s%s" % (self.name.ljust(30), str(self.getCurrent(cam)))



class RangeProperty(object):

    def __init__(self, name, propName, minValue, maxValue, step=1):
        self.minValue = minValue
        self.maxValue = maxValue
        self.name = name
        self.propName = propName
        self.step = step

    def getCurrent(self, cam):
        return getattr(cam, self.propName)

    def increment(self, cam):
        setattr(cam, self.propName, min(
            getattr(cam, self.propName) + self.step, self.maxValue))

    def decrement(self, cam):
        setattr(cam, self.propName, min(
            getattr(cam, self.propName) - self.step, self.maxValue))


class Options(object):

    awbMode = EnumProperty('auto white balance', 'awb_mode', [
                        'off'] + [k for k in PiCamera.AWB_MODES.keys() if k != 'off'])
    exposureMode = EnumProperty('Exposure Modes', 'awb_mode', [
        'off'] + [k for k in PiCamera.EXPOSURE_MODES.keys()if k != 'off'])

    isoModes = EnumProperty('Iso Modes', 'iso', [
                         0, 100, 200, 320, 400, 500, 640, 800])

    camBrightness = RangeProperty("brightness", "brightness", 0, 100)
    camContrast = RangeProperty("contrast", "contrast", 0, 100)
    camSharpness = RangeProperty("sharpness", "sharpness", -100, 100, step=5)
    camSaturation = RangeProperty(
        "saturation", "saturation", -100, 100, step=5)
    camExpCompensation = RangeProperty(
        "exposure compensation", "exposure_compensation", -25, 25)
