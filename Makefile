setup-deps:
	sudo apt-get update
	sudo apt-get upgrade -y
	sudo apt-get install imagemagick python-tornado -y

setup-gpios:
	git clone https://github.com/joan2937/pigpio
	cd pigpio
	make -j4
	sudo make install

install-printer:
	sudo cp 99-escpos.rules /etc/udev/rules.d/
	sudo service udev restart || sudo udevadm control --reload
	git clone http://github.com/mosquito/python-escpos
	cd python-escpos
	python setup.py build
	sudo python setup.py install

install-python-deps:
	sudo pip install -r requirements.txt

setup: setup-deps setup-gpios install-printer install-python-deps


install:
	sudo cp systemd/* /etc/systemd/system/
	sudo systemctl daemon-reload
	sudo systemctl enable fkcontroller
	sudo systemctl enable pigpio
	mkdir -p $$HOME/.fetenkiste/
	cp config.cfg $$HOME/.fetenkiste/

# set time like this:
# timedatectl set-time "2016-01-07 13:45:00"

dev-deploy:
	rsync -av --exclude=.git --exclude=venv --exclude=app --exclude=build . pi@fetenkiste.fritz.box:/home/pi/fetenkiste
	ssh pi@fetenkiste.fritz.box 'cd fetenkiste && make install && sudo systemctl restart fkcontroller'


set-date:
	sudo date --set='16 SEP 2017 21:44'
