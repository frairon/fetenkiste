module.exports = function(grunt) {
    grunt.initConfig ({
        sass: {
            dist: {
                /**
                options: {
                    sourceMap: true,
                    outputStyle: 'compressed'
                }, */
                files: {
                    'public/stylesheets/app.css': 'scss/app.scss'
                }
            }
        },
        watch: {
            css: {
                files: 'scss/**/*.scss',
                tasks: ['sass'],
                options: {
                    livereload: true, // needed to run LiveReload
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['watch']);
}
