var express = require('express');
var fileModule = require('../module/fileModule'),
	config = require('./../config');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var image =  fileModule.images(config.image.dir, function(err, files) {
      res.render('images',
          {
              title: 'Fetenkiste',
              images: files});
  });

});

module.exports = router;
