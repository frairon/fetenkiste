var fs = require('fs'),
    path = require('path');

module.exports = {
    images: function images(imageDir, callback) {
        var fileType = '.jpg',
            files = [], i;
        fs.readdir(imageDir, function (err, list) {
			console.log(err);
            for (i = 0; i < list.length; i++) {
                if (path.extname(list[i]) === fileType) {
                    files.push(list[i]); //store the file name into the array files
                }
            }
            callback(err, files);
        });
    }
};

