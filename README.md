# Fotobox Fetenkiste

Fetenkiste is a tool to turn a Raspberry Pi, the PiCamera and a thermal printer into
a foto-booth.

## How to setup

* get a Raspberry Pi (we use Pi3, but others should work too)
* prepare the OS, probably just a [Raspbian](https://www.raspberrypi.org/downloads/raspbian/)
  * we use the desktop version, but with a bit of modification it might also work without desktop.
* get a PiCamera. Again, we tried different hardware versions and all worked.
* get a display. We used a normal 17'' with resolution 1280x1024
* get a thermal printer. We used Epson TM-T20II.
* get a hardware switch as release-button
* setup the Pi (see "set up the pi" below)
* take pictures and enjoy

## Configuration
Most of the config is done in `config.cfg`. Some settings are hardcoded in the program and must probably be moved to this config at some point.

Setting some options during runtime of the application are stored in an extra ini-file, so the config.cfg is not modified.

## How Fetenkiste works
Some pictures will follow...

* starting the program it connects to the printer and opens up the camera-preview
on the display.
* press the release button
* a countdown will be displayed
* when zero is reached, the camera takes a picture and shows it while it's being printed and saved on disk.
* There is an On-screen-display which you can enter after pressing the admin password (see config).
  * All settings are changed via keyboard, no mouse required.
  * this screen is very basic and just to avoid restarting the app a million times when trying to figure out the right brightness-setting


## Set up the Pi

Most of the dependencies should automatically be set up when running

```
make setup
```

That includes:
* setting up system and python dependencies
* setting up gpiod, a way to control the GPIOs without root
* installing the escpos (printer) library


### PNG Viewer

To view the recently taken picture, we use pngview, a simple utility tool.
Setting this up is not automated yet as the repo seems to have changed recently.

The tool is available here
https://github.com/AndrewFromMelbourne/raspidmx

You need to clone and then compile the file in `pngview/pngview.c`.

## Starting Fetenkiste

Currently there is no automated way. On every system start you need to start the pigpiod-daemon to provide access to the hardware pins. Feel free to add an autostart-script for that.

Same goes for Fetenkiste. There is no autostart yet, but feel free to run it automatically on startup.

## Known Issues

### Date and Time on the raspi

The raspberry pi has no battery, so it doesn't know the current time. If you have no internet connection, it will have the same time after every restart.
To get a more or less reliable picture order for the saved images, a picture counter is added to the filenames.

Set the time manually with something like this if you like:
```
  timedatectl set-time "2016-01-07 13:45:00"
```
### Application Hang
After certain amount of time or number of pictures, the whole application seems to hang.
Overheating may be the reason, but we didn't do a real troubleshooting yet. Only possible solution is a restart.

### Flipped images
For convenience, the camera-preview is displayed in a mirror-like feeling. To get a correct print, the picture is flipped prior to printing. The images saved to disk are still in the wrong direction and should be flipped.

### Brightness settings

For bad lighting it is quite hard to get nice brightness settings for
* displaying the preview
* the printed image
* the saved image

One of the three is always too dark or too bright. Good luck.

## Viewer App
A skeleton app to view the images taken in the foto-booth via web is implemented in the viewer-app. It was never finished, so just ignore it.

### requirements
- Node.js with npm package manager
- sass compiler

### setup
set image directory and port in `config.js`.
run following commands
```
npm install
grunt sass
npm start
```
go to `http://localhost:[port]/fetenkiste`


### grunt watcher
- watch task for sass compiler `grunt watch`
